<?php

use yii\db\Migration;

class m170424_094645_create_table_master_data extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        
        }
        $this->createTable('{{%master_data}}', [
            'id' => $this->primaryKey(),
            'produk' => $this->string(100)->notNull(),
            'description' => $this->string(100)->notNull(),
            'batch' => $this->string(50)->notNull(),
            'hu_number' => $this->text(),
            'quantity' => $this->integer(50)->notNull(),
            'hu_number_key' => $this->integer(50),
            'status' => $this->string(50),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('master_data');
        echo "m170424_094645_create_table_master_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
