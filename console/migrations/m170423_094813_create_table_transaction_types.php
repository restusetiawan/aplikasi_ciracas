<?php

use yii\db\Migration;
use yii\db\Schema;

class m170423_094813_create_table_transaction_types extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        
        }
        $this->createTable('{{%transaction_types}}', [
            'id' => $this->primaryKey(),
            'code' => $this->string(50)->notNull(),
            'name' => $this->string(100)->notNull(),
        ]);

        $this->execute($this->insertTypes());
    }

    private function insertTypes()
    {
        $this->batchInsert('{{%transaction_types}}', ['id', 'code', 'name'], 
            [
                ['1', 'IN', 'Barang masuk'],
                ['2', 'OUT', 'Barang keluar'],
            ]);
    }

    public function down()
    {
        $this->dropTable('transaction_types');
        echo "m170423_094813_create_table_transaction_types cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
