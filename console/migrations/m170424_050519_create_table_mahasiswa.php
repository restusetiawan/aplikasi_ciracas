<?php

use yii\db\Migration;

class m170424_050519_create_table_mahasiswa extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        
        }
        $this->createTable('{{%mahasiswa}}', [
            'id' => $this->primaryKey(),
            'nim' => $this->string(100)->notNull(),
            'nama' => $this->string(50)->notNull(),
            'jurusan' => $this->string(100)->notNull(),
            'angkatan' => $this->integer(10)->notNull(),
            'alamat' => $this->string(100),
        ], $tableOptions);
    }

    public function down()
    {

        $this->dropTable('mahasiswa');
        echo "m170424_050519_create_table_mahasiswa cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
