<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterData;

/**
 * MasterDataSearch represents the model behind the search form about `common\models\MasterData`.
 */
class MasterDataSearch extends MasterData
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'quantity', 'hu_number_key'], 'integer'],
            [['activity', 'type_produk', 'produk', 'description', 'batch', 'hu_number', 'no_sto', 'order_no', 'no_pol', 'driver_name', 'status', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterData::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'quantity' => $this->quantity,
            'hu_number_key' => $this->hu_number_key,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'activity', $this->activity])
            ->andFilterWhere(['like', 'type_produk', $this->type_produk])
            ->andFilterWhere(['like', 'produk', $this->produk])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'batch', $this->batch])
            ->andFilterWhere(['like', 'hu_number', substr($this->hu_number, -14)])
            ->andFilterWhere(['like', 'no_sto', $this->no_sto])
            ->andFilterWhere(['like', 'order_no', $this->order_no])
            ->andFilterWhere(['like', 'no_pol', $this->no_pol])
            ->andFilterWhere(['like', 'driver_name', $this->driver_name])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
