<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterNotes;

/**
 * MasterNotesSearch represents the model behind the search form about `common\models\MasterNotes`.
 */
class MasterNotesSearch extends MasterNotes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_notes'], 'integer'],
            [['no_refrence', 'notes', 'create_at', 'update_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterNotes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_notes' => $this->id_notes,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
        ]);

        $query->andFilterWhere(['like', 'no_refrence', $this->no_refrence])
            ->andFilterWhere(['like', 'notes', $this->notes]);

        return $dataProvider;
    }
}
