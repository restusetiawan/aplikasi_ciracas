<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%master_lokasi}}".
 *
 * @property integer $lokasi_id
 * @property string $lokasi_name
 * @property string $lokasi_type
 * @property string $create_at
 * @property string $update_at
 */
class MasterLokasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%master_lokasi}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['create_at', 'update_at'], 'safe'],
            [['lokasi_name', 'lokasi_type'], 'required'],
            [['lokasi_name', 'lokasi_type'], 'string', 'max' => 50],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_at',
                'updatedAtAttribute' => 'update_at',
                'value'=> new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lokasi_id' => Yii::t('app', 'Lokasi ID'),
            'lokasi_name' => Yii::t('app', 'Lokasi Name'),
            'lokasi_type' => Yii::t('app', 'Lokasi Type'),
            'create_at' => Yii::t('app', 'Create At'),
            'update_at' => Yii::t('app', 'Update At'),
        ];
    }
}
