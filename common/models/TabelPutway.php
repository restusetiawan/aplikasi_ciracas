<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%tabel_putway}}".
 *
 * @property integer $putway_id
 * @property string $hu_number
 * @property string $to_lokasi
 * @property string $to_row
 * @property integer $to_level
 * @property string $refrence
 * @property string $create_at
 * @property string $update_at
 */
class TabelPutway extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tabel_putway}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_at',
                'updatedAtAttribute' => 'update_at',
                'value'=> new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hu_number'], 'string'],
            [['hu_number', 'to_lokasi', 'to_row', 'to_level', 'type_produk'], 'required'],
            [['hu_number'], 'unique'],
            [['to_level'], 'integer'],
            [['create_at', 'update_at'], 'safe'],
            [['to_lokasi', 'to_row', 'status'], 'string', 'max' => 50],
            [['refrence'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'putway_id' => Yii::t('app', 'Putway ID'),
            'type_produk' => Yii::t('app', 'Type Produk'),
            'hu_number' => Yii::t('app', 'Hu Number'),
            'to_lokasi' => Yii::t('app', 'To Lokasi'),
            'to_row' => Yii::t('app', 'To Row'),
            'to_level' => Yii::t('app', 'To Level'),
            'refrence' => Yii::t('app', 'Refrence'),
            'status' => Yii::t('app', 'Status'),
            'create_at' => Yii::t('app', 'Create At'),
            'update_at' => Yii::t('app', 'Update At'),
        ];
    }
}
