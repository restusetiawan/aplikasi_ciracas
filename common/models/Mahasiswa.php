<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mahasiswa".
 *
 * @property integer $id
 * @property string $nim
 * @property string $nama
 * @property string $jurusan
 * @property integer $angkatan
 * @property string $alamat
 */
class Mahasiswa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mahasiswa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nim', 'nama', 'jurusan', 'angkatan'], 'required'],
            [['angkatan'], 'integer'],
            [['nim', 'jurusan', 'alamat'], 'string', 'max' => 100],
            [['nama'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nim' => Yii::t('app', 'Nim'),
            'nama' => Yii::t('app', 'Nama'),
            'jurusan' => Yii::t('app', 'Jurusan'),
            'angkatan' => Yii::t('app', 'Angkatan'),
            'alamat' => Yii::t('app', 'Alamat'),
        ];
    }
}
