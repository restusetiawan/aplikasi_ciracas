<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "transporter".
 *
 * @property integer $trans_id
 * @property string $name
 * @property string $no_pol
 * @property string $type_truck
 * @property string $create_at
 * @property string $update_at
 */
class Transporter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transporter';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_at',
                'updatedAtAttribute' => 'update_at',
                'value'=> new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['create_at', 'update_at'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['no_pol', 'type_truck'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'trans_id' => Yii::t('app', 'Trans ID'),
            'name' => Yii::t('app', 'Name'),
            'no_pol' => Yii::t('app', 'No Pol'),
            'type_truck' => Yii::t('app', 'Type Truck'),
            'create_at' => Yii::t('app', 'Create At'),
            'update_at' => Yii::t('app', 'Update At'),
        ];
    }
}
