<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%master_data}}".
 *
 * @property integer $id
 * @property string $activity
 * @property string $type_produk
 * @property string $produk
 * @property string $description
 * @property string $batch
 * @property string $hu_number
 * @property integer $quantity
 * @property integer $hu_number_key
 * @property string $no_pol
 * @property string $driver_name
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 */
class MasterData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%master_data}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value'=> new Expression('NOW()'),
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['produk', 'description', 'batch', 'quantity'], 'required'],
            [['hu_number'], 'string'],
            [['hu_number'], 'unique'],
            [['quantity', 'hu_number_key'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['activity', 'type_produk', 'batch', 'no_pol', 'order_no', 'status'], 'string', 'max' => 50],
            [['produk', 'description', 'driver_name', 'no_sto'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'activity' => Yii::t('app', 'Activity'),
            'type_produk' => Yii::t('app', 'Type Produk'),
            'produk' => Yii::t('app', 'Produk'),
            'description' => Yii::t('app', 'Description'),
            'batch' => Yii::t('app', 'Batch'),
            'hu_number' => Yii::t('app', 'Hu Number'),
            'quantity' => Yii::t('app', 'Quantity'),
            'hu_number_key' => Yii::t('app', 'Hu Number Key'),
            'no_sto' => Yii::t('app', 'No STO'),
            'order_no' => Yii::t('app', 'Order No'),
            'no_pol' => Yii::t('app', 'No Pol'),
            'driver_name' => Yii::t('app', 'Driver Name'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
