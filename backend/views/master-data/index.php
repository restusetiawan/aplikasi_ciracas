<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\builder\TabularForm;
use yii\widgets\ActiveForm;
//use kartik\grid\GridView;
//use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterDataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Result Outbound');
$this->params['breadcrumbs'][] = $this->title;
?>
<script type="text/javascript">
      $(document).on('click', '.showModalButton', function(){
        if ($('#modal').data('bs.modal').isShown) {
            $('#modal').find('#modalContent')
                    .load($(this).attr('value'));
            document.getElementById('modalHeader').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
        } else {
            $('#modal').modal('show')
                    .find('#modalContent')
                    .load($(this).attr('value'));
            document.getElementById('modalHeader').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
        }
    });
</script>
<div class="master-data-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?php echo Html::button(Yii::t('app', 'Upload Data Outbound'), ['value' => Url::to(['site/upload']), 
            'class' =>  ' showModalButton btn btn-success', 'id' => 'modalButton'
        ]) ?>
    </p>
    <p>
        <span class="label label-success">1</span>  <b>New</b>
        <span class="label label-warning">2</span>  <b>Scan</b>
        <span class="label label-primary">3</span>  <b>Confirm</b>
        <span class="label label-danger">4</span>   <b>Shipment</b>
        <span class="label label-default">5</span>  <b>Done</b>
    </p>
<?php 
Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'header'=> '<h3>Upload Data</h3>',
    'id' => 'modal',
    'size' => 'modal-lg',
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>    
<?php Pjax::begin(); ?>
<?= Html::beginForm(['master-data/delete_all'], 'post'); ?>
<?php 
$gridColumns = [
    [
        'class' => '\kartik\grid\SerialColumn'
    ],
    [
        'class'       => '\kartik\grid\CheckboxColumn',
        'pageSummary' => true,
        'rowSelectedClass' => GridView::TYPE_DANGER,
    ],
    [
        'attribute' => 'status',
        'format' => 'raw',
        'filter' => \kartik\widgets\Select2::widget([
            'model' => $searchModel,
            'attribute' => 'status',
            'data' => [
                'new' => '1',
                'scan' => '2',
                'confirm' => '3',
                'Shipment' => '4',
                'done' => '5'
            ],
            'options' => ['placeholder' => 'Select'],
            'pluginOptions' => ['allowClear' => true]
        ]),
        'value' => function ($model) {
            if ($model->status === 'scan') {
                return "<span class='label label-warning'>2</span>";
            } elseif ($model->status === 'new') {
                return "<span class='label label-success'>1</span>";
            } elseif($model->status === 'confirm') {
                return "<span class='label label-primary'>3</span>";
            } elseif($model->status === 'shipment') {
                return "<span class='label label-danger'>4</span>";
            } else {
                return "<span class='label label-default'>5</span>";
            }
        },
    ],
    [
        'attribute' => 'type_produk',
        'filter' => \kartik\widgets\Select2::widget([
            'model' => $searchModel,
            'attribute' => 'type_produk',
            'data' => \yii\helpers\ArrayHelper::map(\common\models\MasterType::find()->all(),'produk_type', 'produk_type'),
            'options' => ['placeholder' => 'Select'],
            'pluginOptions' => ['allowClear' => true]
        ]),
    ],
    'produk',
    'description', 
    'hu_number', 
    'batch', 
    'quantity',
    'no_sto',
    'order_no',
    'no_pol',
    'driver_name',
    ['class' => '\kartik\grid\ActionColumn'],
];
echo GridView::widget([
        'dataProvider'  => $dataProvider,
        'filterModel'   => $searchModel,
        'columns'       => $gridColumns,
        'responsive'    =>true,
        'hover'         =>true,
        'pjax'          =>true,
        'striped'       => true,
        'pjaxSettings'  =>[
            'neverTimeout' => true,
        ],
        'floatHeader'=>false,
        'floatHeaderOptions'=>['scrollingTop'=>'50'],
    ]);
?>
<div class="form-group">
    <?= Html::submitButton('<i class="fa fa-trash"></i>  Delete Selection', [ 'id' => 'delete', 'class' => 'btn btn-danger btn-sm']) ?>
</div>
<?= Html::endForm(); ?>
<?php Pjax::end(); ?></div>