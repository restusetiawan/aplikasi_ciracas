<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\MasterData */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-data-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'activity')->widget(Select2::classname(), [
            'name' => 'no_sto',
                'data' => [
                    'inbound' => 'inbound',
                    'outbound' => 'outbound',
            ],
            'options' => [
                'placeholder' => '--pilih activity--',
                'multiple' => false
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
    ]) ?>
    
    <?= 
        $form->field($model, 'type_produk')->widget(Select2::classname(), [
            'name' => 'no_sto',
            'data' => \yii\helpers\ArrayHelper::map(\common\models\MasterType::find()->all(),'produk_type', 'produk_type'),
            'options' => [
                'placeholder' => '--pilih Type produk--',
                'multiple' => false
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
    ?>

    <?= $form->field($model, 'produk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'batch')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hu_number')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'quantity')->textInput() ?>

    <?= $form->field($model, 'hu_number_key')->textInput() ?>
    
    <?= $form->field($model, 'no_sto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'order_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_pol')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'driver_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->widget(Select2::classname(), [
            'name' => 'no_sto',
                'data' => [
                    'new' => 'new',
                    'scan' => 'scan',
                    'confirm' => 'confirm',
                    'shipment' => 'shipment',
                    'done' => 'done'
            ],
            'options' => [
                'placeholder' => '--pilih status--',
                'multiple' => false
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
