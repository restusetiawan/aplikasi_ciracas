<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\Events */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="events-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'start_date')->widget(\yii\jui\DatePicker::classname(), [
		'options' => ['class' => 'form-control'],
		'dateFormat' => 'yyyy-MM-dd',
		'clientOptions' =>[
		'numberOfMonths' => 1,
		'minDate' => '-1m',
		'maxDate' => '+1m',
		'changeMonth' => true,
		]
	]) ?>

    <?= $form->field($model, 'end_date')->widget(\yii\jui\DatePicker::classname(), [
		'options' => ['class' => 'form-control'],
		'dateFormat' => 'yyyy-MM-dd',
		'clientOptions' =>[
		'numberOfMonths' => 1,
		'minDate' => '-1m',
		'maxDate' => '+1m',
		'changeMonth' => true,
		]
	]) ?>

	<?= $form->field($model, 'publish')->widget(SwitchInput::classname(),
    [
        'pluginOptions'=>[
            'onText'=>'Publish',
            'offText'=>'Hidden',
        ],
    ]);
    ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
