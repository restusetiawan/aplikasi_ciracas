<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterType */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Master Type',
]) . $model->id_type;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_type, 'url' => ['view', 'id' => $model->id_type]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="master-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
