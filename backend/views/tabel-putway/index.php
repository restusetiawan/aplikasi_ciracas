<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\TabelPutwaySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tabel Putways');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tabel-putway-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<?= Html::beginForm(['tabel-putway/edit_refrence'], 'post'); ?>
    <p>
        <?= Html::a(Yii::t('app', 'Create Tabel Putway'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::submitButton('Update Selection', [ 'id' => 'delete', 'class' => 'btn btn-primary']) ?>
    </p>
    <div class="row">
        <div class="col-md-4">
            <input type="text" class="form-control" placeholder="Please entry your refrence" required name="refrence">
        </div>
    </div>
<?php Pjax::begin(); ?>
<?php 
$gridColumns = [
    [
        'class' => '\kartik\grid\SerialColumn'
    ],
    [
        'class'       => '\kartik\grid\CheckboxColumn',
        'pageSummary' => true,
        'rowSelectedClass' => GridView::TYPE_DANGER,
    ],
    [
        'attribute' => 'type_produk',
        'filter' => \kartik\widgets\Select2::widget([
            'model' => $searchModel,
            'attribute' => 'type_produk',
            'data' => \yii\helpers\ArrayHelper::map(\common\models\MasterType::find()->all(),'produk_type', 'produk_type'),
            'options' => ['placeholder' => 'Select'],
            'pluginOptions' => ['allowClear' => true]
        ]),
    ],
    'hu_number:ntext',
    'to_lokasi',
    'to_row',
    'to_level',
    'refrence',
    [
        'attribute' => 'status',
        'format' => 'raw',
        'filter' => \kartik\widgets\Select2::widget([
            'model' => $searchModel,
            'attribute' => 'status',
            'data' => [
                'new' => 'new',
                'scan' => 'scan',
                'confirm' => 'confirm',
                'done' => 'done'
            ],
            'options' => ['placeholder' => 'Select'],
            'pluginOptions' => ['allowClear' => true]
        ]),
        'value' => function ($model) {
            if ($model->status === 'scan') {
                return "<span class='label label-warning'>Scann</span>";
            } elseif ($model->status === 'new') {
                return "<span class='label label-success'>New</span>";
            } elseif($model->status === 'confirm') {
                return "<span class='label label-primary'>Confirm</span>";
            } else {
                return "<span class='label label-default'>done</span>";
            }
        },
    ],
    ['class' => '\kartik\grid\ActionColumn'],
];
?>    
<?= GridView::widget([
        'dataProvider'  => $dataProvider,
        'filterModel'   => $searchModel,
        'columns'       => $gridColumns,
        'rowOptions'    => function ($model) {
            if ($model->status == 'scan') {
                return ['class' => 'warning'];
            }
        },
        'responsive'    =>true,
        'hover'         =>true,
        'pjax'          =>true,
        'striped'       => true,
        'pjaxSettings'  =>[
            'neverTimeout' => true,
        ],
        'floatHeader'=>false,
        'floatHeaderOptions'=>['scrollingTop'=>'50'],
    ]); ?>

<?php Pjax::end(); ?>
<?= Html::endForm(); ?>
</div>
