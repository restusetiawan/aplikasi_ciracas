<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\TabelPutway */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tabel-putway-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?php 
        $type = \common\models\MasterType::find()->all();
        $listType = \yii\helpers\ArrayHelper::map($type, 'produk_type', 'produk_type');
    ?>
    
    <?= $form->field($model, 'type_produk')->dropDownList($listType, ['prompt' => 'Pilih Produk']) ?>

    <?= $form->field($model, 'hu_number')->textInput() ?>
    
    <?php 
        $lokasi = \common\models\MasterLokasi::find()->orderBy('lokasi_name ASC')->all();
        $listLokasi = \yii\helpers\ArrayHelper::map($lokasi, 'lokasi_name', 'lokasi_name'); 
    ?>

    <?= $form->field($model, 'to_lokasi')->textInput() ?>

    <?php 
        $row = \common\models\MasterRow::find()->orderBy('name_row ASC')->all();
        $listRow = \yii\helpers\ArrayHelper::map($row, 'name_row', 'name_row'); 
    ?>    

    <?= $form->field($model, 'to_row')->dropDownList($listRow, ['prompt' => 'Pilih Row']) ?>

    <?= $form->field($model, 'to_level')->textInput() ?>

    <?php //$form->field($model, 'refrence')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
