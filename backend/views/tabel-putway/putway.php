<?php 

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\grid\GridView;

$this->title = Yii::t('app', 'Putway Confirm');
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= \yii\helpers\Html::encode($this->title); ?></h1>
<?= Html::beginForm(['tabel-putway/putway_confirmation'], 'post'); ?>
<div class="form-group">
	<input type="text" class="form-control" name="refrence" placeholder="Refrence" required >
</div>
<?php 
	$gridColumns = [
	    [
	        'class' => '\kartik\grid\SerialColumn'
	    ],
	    [
	        'class'       => '\kartik\grid\CheckboxColumn',
	        'pageSummary' => true,
	        'rowSelectedClass' => GridView::TYPE_DANGER	,
	    ],
	    'type_produk',
	    'hu_number', 
	    'to_lokasi',
	    'to_row',
	    ['class' => '\kartik\grid\ActionColumn'],
    ];
	echo GridView::widget([
	    'dataProvider' 	=> $dataProvider,
	    //'filterModel'  	=> $searchModel,
	    'columns'      	=> $gridColumns,
	    'responsive'   	=>true,
	    'hover'        	=>true,
	    'pjax'         	=>true,
	    'striped'		=> true,
	    'pjaxSettings' 	=>[
	        'neverTimeout' => true,
	//      'beforeGrid'   => 'My fancy content before.',
	//      'afterGrid'    => 'My fancy content after.',
	    ],
	    'floatHeader'=>false,
	    'floatHeaderOptions'=>['scrollingTop'=>'50'],
	//  'showPageSummary' => true,
	]); 
?>
<?= Html::submitButton('Confirm', [ 'id' => 'update', 'class' => 'btn btn-success']) ?>
<?= Html::endForm(); ?>