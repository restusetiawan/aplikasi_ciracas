<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MasterDo */

$this->title = $model->do_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Dos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-do-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->do_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->do_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'do_id',
            'no_do',
            'create_at',
            'update_at',
        ],
    ]) ?>

</div>
