<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterDo */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Master Do',
]) . $model->do_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Dos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->do_id, 'url' => ['view', 'id' => $model->do_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="master-do-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
