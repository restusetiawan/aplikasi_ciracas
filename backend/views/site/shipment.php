<?php 

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use kartik\widgets\Select2;

$this->title = Yii::t('app', 'Shipment Confirmastion');
$this->params['breadcrumbs'][] = $this->title;

?>
<h1><?= \yii\helpers\Html::encode($this->title); ?></h1>
<?php echo $this->render('_shipment', ['model' => $searchModel]); ?>
<?= Html::beginForm(['site/shipment'], 'post'); ?>
<?php 
	if ($searchModel->no_sto !== null) {

		$gridColumns = [
		    [
		        'class' => '\kartik\grid\SerialColumn'
		    ],
		    [
		        'class'       => '\kartik\grid\CheckboxColumn',
		        'pageSummary' => true,
		        'rowSelectedClass' => GridView::TYPE_DANGER	,
		    ],
		    'produk',
		    'hu_number', 
		    'batch', 
		    'quantity',
	    ];
	    $truck = \common\models\Transporter::find()->all();
		$listTruck = \yii\helpers\ArrayHelper::map($truck, 'no_pol', 'no_pol');
		echo "<div class='row'>";
			echo "<div class='col-md-4'>";
				echo "<div class='form-group'>";
					echo Select2::widget([
						'name' => 'no_pol',
						'data' => $listTruck,
						'value' => '',
						'options' => ['placeholder' => 'Pilih No Polisi'],
						'pluginOptions' => ['allowClear' => true],
					]);
				echo "</div>";
			echo "</div>";
			echo "<div class='col-md-4'>";
				echo "<div class='form-group'>";
					echo "<input type='text' class='form-control' name='driver_name' placeholder='Nama Supir' required>";
				echo "</div>";
			echo "</div>";
		echo "</div>";

		echo GridView::widget([
		    'dataProvider' 	=> $dataProvider,
		    //'filterModel'  	=> $searchModel,
		    'columns'      	=> $gridColumns,
		    'responsive'   	=>true,
		    'hover'        	=>true,
		    'pjax'         	=>true,
		    'striped'		=> true,
		    'pjaxSettings' 	=>[
		        'neverTimeout' => true,
		//      'beforeGrid'   => 'My fancy content before.',
		//      'afterGrid'    => 'My fancy content after.',
		    ],
		    'floatHeader'=>false,
		    'floatHeaderOptions'=>['scrollingTop'=>'50'],
		//  'showPageSummary' => true,
		]);
		echo Html::submitButton('Confirm', [ 'id' => 'update', 'class' => 'btn btn-success']);
	}
?>
<?= Html::endForm(); ?>