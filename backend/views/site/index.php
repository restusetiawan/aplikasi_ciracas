<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap\Progress;
/* @var $this yii\web\View */

$this->title = 'Dashboard';
?>
<div class="site-index">
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <div class="row">
                    <div class="col-md-3">
                    <h4 align="center">New</h4>
                        <?php 
                            $count = common\models\MasterData::find()->where(['status' => 'new'])->count();
                            echo "<h3 align='center'>$count</h3>";
                            echo "<p align='center'><i>pallet</i></p>";
                        ?>
                    </div>
                    <div class="col-md-3">
                        <h4 align="center">Scan</h4>
                        <?php 
                            $count = common\models\MasterData::find()->where(['status' => 'scann'])->count();
                            echo "<h3 align='center'>$count</h3>";
                            echo "<p align='center'><i>pallet</i></p>";
                        ?>
                    </div>
                    <div class="col-md-3">
                        <h4 align="center">Confirm</h4>
                        <?php 
                            $count = common\models\MasterData::find()->where(['status' => 'confirm'])->count();
                            echo "<h3 align='center'>$count</h3>";
                            echo "<p align='center'><i>pallet</i></p>";
                        ?>
                    </div>
                    <div class="col-md-3">
                        <h4 align="center">Shipment</h4>
                        <?php 
                            $count = common\models\MasterData::find()->where(['status' => 'shipment'])->count();
                            echo "<h3 align='center'>$count</h3>";
                            echo "<p align='center'><i>pallet</i></p>";
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label>Outbound proses</label>
            <?php
                $date = date("Y-m-d");
                $new = "new";
                $done = "done";
                $countAll = \common\models\MasterData::find()
                    ->where(['LIKE', 'created_at', $date])
                    ->count();
                $countNew = \common\models\MasterData::find()
                    ->where(['status' => $new,])
                    ->andWhere(['LIKE', 'created_at', $date])
                    ->count();
                $countDone = \common\models\MasterData::find()
                    ->where(['status' => $done])
                    //->andWhere(['LIKE', 'created_at', $date])
                    ->count();
                if ($countNew != 0) {
                    $percent = round($countDone/$countAll * 100, 2);
                    // default with label
                    echo "<b>( $percent % )</b>";
                    echo Progress::widget([
                        'percent' => $percent,
                        'label' => '',
                        'barOptions' => ['class' => 'progress-bar-warning'],
                        'options' => ['class' => 'active progress-striped']
                    ]);
                } else {
                    echo Progress::widget([
                        'percent' => 0,
                        'label' => '0 %',
                        //'barOptions' => ['class' => 'progress-bar-warning'],
                        //'options' => ['class' => 'active progress-striped']
                    ]);
                }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= \yii2fullcalendar\yii2fullcalendar::widget(array(
                        'events'=> $events,
                    )); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <h4>Confirm status</h4>
            </div>
            <div class="form-group">
                <?= Html::beginForm(['site/index'], 'post', ['data-pjax' => '', 'class' => 'form-inline']); ?>
                <?php
                    echo yii\jui\DatePicker::widget([
                        'options' => ['class' => 'form-control'],
                        'value' => $day,
                        'name' => 'datepicker',
                        'dateFormat' => 'yyyy-MM-dd',
                        'clientOptions' =>[
                            'numberOfMonths' => 1,
                            //'minDate' => '-1m',
                            //'maxDate' => '+1m',
                            'changeMonth' => true,
                        ]
                ]) ?>
                <?= Html::submitButton('<i class="fa fa-search"></i>', ['class' => 'btn btn-primary', 'name' => 'hash-button']) ?>
                <?= Html::endForm() ?>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php 

                    $gridColumns = [
                        [
                            'class' => '\kartik\grid\SerialColumn'
                        ],
                        'type_produk',
                        'batch',
                        'hu_number',
                        'quantity',
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model) {
                                if ($model->status == 'confirm') {
                                    return "<i class='fa fa-check' style='color:green;'></i>";
                                }
                            },
                        ],
                    ];
                    echo GridView::widget([
                        'dataProvider'  => $dataProviderFg,
                        'columns'       => $gridColumns,
                        'responsive'    =>true,
                        'hover'         =>true,
                        'pjax'          =>true,
                        'striped'       => true,
                        'pjaxSettings'  =>[
                            'neverTimeout' => true,
                        ],
                        'floatHeader'=>false,
                        'floatHeaderOptions'=>['scrollingTop'=>'50'],
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
