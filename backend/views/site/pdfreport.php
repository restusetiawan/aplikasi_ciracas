<p style="margin-left: 7cm;">PT. KAMADJAJA LOGISTICS</p>
<p style="margin-left: 7.6cm;"><b>DELIVERY ORDER</b></p>
<table style="width: 100%";>
	<tr>
		<td>STO Number</td>
		<td>:</td>
		<td><h5><b><?php echo $no_sto; ?></b></h5></td>
		<td>Delivery Date</td>
		<td>:</td>
		<td><?php echo date("d/m/Y"); ?></td>
	</tr>
	<tr>
		<td>Truck No</td>
		<td>:</td>
		<td><?php echo $no_pol; ?></td>
		<td>Driver Name</td>
		<td>:</td>
		<td><?php echo $nama_supir; ?></td>
	</tr>
	<tr>
		<td>Type truck</td>
		<td>:</td>
		<td><?php echo $jenis_truk; ?></td>
		<td>Transporter</td>
		<td>:</td>
		<td><?php echo $transporter; ?></td>
	</tr>
</table>
<br>
<table>
	<tr>
		<td><b>On Behalf of :<b></td>
		<td><b>Delivered To :</b></td>
	</tr>
	<tr>
		<td>PT. Kamadjaja Logistics,
			Jln Kiwi, Pergudangan Karplindo
			Ciracas, Jakarta Timur
		</td>
		<td><?php echo $address; ?></td>
	</tr>
</table>
<br><br>
<table style="border-collapse: collapse; width: 100%;">
	<tr style="background-color: #F2F2F2;">
		<th style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">No</th>
		<th style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">Produk</th>
		<th style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">Description</th>
		<th style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">Batch Number</th>
		<th style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">HU Number</th>
		<th style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd;">Quantity</th>
	</tr>
	<tbody>
		<?php 
		$no = 1;
		foreach ($models as $model) { ?>
		<tr>
			<td style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd;"><?php echo $no; ?></td>
			<td style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd;"><?php echo $model->produk; ?></td>
			<td style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd;"><?php echo $model->description; ?></td>
			<td style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd;"><?php echo $model->batch; ?></td>
			<td style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd;"><?php echo $model->hu_number; ?></td>
			<td style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd;"><?php echo $model->quantity; ?></td>
		</tr>
		<?php $no++; } ?>
	</tbody>
</table>
<br>
<table style="width: 100%;">
	<tr>
		<td>Masuk :</td>
		<td>Start :</td>
		<td>Finish :</td>
		<td>Keluar :</td>
	</tr>
</table>
<table style="border-collapse: collapse; width: 100%; margin-top: 5cm;">
	<tr>
		<td style=" height: 1cm; width: 30%;"><b>Admin Warehouse :</b></td>
		<td style=" height: 1cm; width: 30%;"><b>Driver Name :</b> <?php echo $nama_supir; ?></td>
		<td style=" height: 1cm; width: 30%;"><b>Spv/Asst. Mgr :</b> Restu / Eddy</td>
	</tr>
</table>