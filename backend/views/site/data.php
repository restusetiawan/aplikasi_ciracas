<?php 

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\grid\GridView;

$this->title = Yii::t('app', 'Data Scann');
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= \yii\helpers\Html::encode($this->title); ?></h1>
<?php echo Yii::t('app', '<p>Please field entry at bellow before confirm</p>') ?>
<?= Html::beginForm(['site/data'], 'post'); ?>
<div class="row">
	<!--<div class="col-md-4">
		<div class="form-group">
			<?php 
				//$truck = \common\models\Transporter::find()->all();
				//$listTruck = \yii\helpers\ArrayHelper::map($truck, 'no_pol', 'no_pol');

				//echo Select2::widget([
				//	'name' => 'no_pol',
				//	'data' => $listTruck,
				//	'value' => '',
				//	'options' => ['placeholder' => 'Pilih No Polisi'],
				//	'pluginOptions' => ['allowClear' => true],
				//])
			?>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<input type="text" class="form-control" name="driver_name" placeholder="Nama Supir" required>
		</div>
	</div>-->
	<div class="col-md-4">
		<div class="form-group">
			<input type="text" class="form-control" name="no_sto" placeholder="No STO" required>
		</div>
	</div>
</div>
<?php 
	$gridColumns = [
	    [
	        'class' => '\kartik\grid\SerialColumn'
	    ],
	    [
	        'class'       => '\kartik\grid\CheckboxColumn',
	        'pageSummary' => true,
	        'rowSelectedClass' => GridView::TYPE_DANGER	,
	    ],
	    'produk',
	    'description', 
	    'hu_number', 
	    'batch', 
	    'quantity',
    ];
	echo GridView::widget([
	    'dataProvider' 	=> $dataProvider,
	    //'filterModel'  	=> $searchModel,
	    'columns'      	=> $gridColumns,
	    'responsive'   	=>true,
	    'hover'        	=>true,
	    'pjax'         	=>true,
	    'striped'		=> true,
	    'export'		=>false,
	    'pjaxSettings' 	=>[
	        'neverTimeout' => true,
	//      'beforeGrid'   => 'My fancy content before.',
	//      'afterGrid'    => 'My fancy content after.',
	    ],
	    'floatHeader'=>false,
	    'floatHeaderOptions'=>['scrollingTop'=>'50'],
	//  'showPageSummary' => true,
	]); 
?>
<?= Html::submitButton('Confirm', [ 'id' => 'update', 'class' => 'btn btn-success']) ?>
<?= Html::endForm(); ?>