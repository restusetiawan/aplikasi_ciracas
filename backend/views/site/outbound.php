<?php 

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;

$this->title = Yii::t('app', 'Outbound Finish Good');
$this->params['breadcrumbs'][] = $this->title;

?>
<h1><?= \yii\helpers\Html::encode($this->title); ?></h1>
<?= Html::beginForm(['site/outbound'], 'post'); ?>
	<?php 
	$gridColumns = [
	    [
	        'class' => '\kartik\grid\SerialColumn'
	    ],
	    [
	        'class'       => '\kartik\grid\CheckboxColumn',
	        'pageSummary' => true,
	        'rowSelectedClass' => GridView::TYPE_DANGER	,
	    ],
	    'produk',
	    'hu_number', 
	    'batch', 
	    'quantity',
	    'no_sto',
	    'driver_name',
	    'no_pol'
    ];
	echo GridView::widget([
	    'dataProvider' 	=> $dataProvider,
	    //'filterModel'  	=> $searchModel,
	    'columns'      	=> $gridColumns,
	    'responsive'   	=>true,
	    'hover'        	=>true,
	    'pjax'         	=>true,
	    'striped'		=> true,
	    'pjaxSettings' 	=>[
	        'neverTimeout' => true,
	//      'beforeGrid'   => 'My fancy content before.',
	//      'afterGrid'    => 'My fancy content after.',
	    ],
	    'floatHeader'=>false,
	    'floatHeaderOptions'=>['scrollingTop'=>'50'],
	//  'showPageSummary' => true,
	]); 
?>
<?= Html::submitButton('Confirm', [ 'id' => 'update', 'class' => 'btn btn-success']) ?>
<?= Html::endForm(); ?>