<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(); ?>
<table class="table table-hover table-bordered table-striped">
	<tr>
		<td><b>Produk</b></td>
		<td><?php echo $form->field($model, 'produk')->hiddenInput([
				'value' => $model['produk'],
				'readOnly' => true
			])->label(false) ?>
			<?php echo $model->produk; ?>
		</td>
	</tr>
	<tr>
		<td><b>Description</b></td>
		<td><?php echo $form->field($model, 'description')->hiddenInput([
				'value' => $model['description'],
				'readOnly' => true
			])->label(false) ?>
			<?php echo $model->description; ?>
		</td>
	</tr>
	<tr>
		<td><b>Hu NUmber</b></td>
		<td><?php echo $form->field($model, 'hu_number')->hiddenInput([
				'value' => $model['hu_number'],
				'readOnly' => true
			])->label(false) ?>
			<?php echo $model->hu_number; ?>
		</td>
	</tr>
	<tr>
		<td><b>Batch Number</b></td>
		<td><?php echo $form->field($model, 'batch')->hiddenInput([
				'value' => $model['batch'],
				'readOnly' => true
			])->label(false) ?>
			<?php echo $model->batch; ?>
		</td>
	</tr>
	<tr>
		<td><b>Quantity</b></td>
		<td><?php echo $form->field($model, 'quantity')->hiddenInput([
				'value' => $model['quantity'],
				'readOnly' => true
			])->label(false) ?>
			<?php echo $model->quantity; ?>
		</td>
	</tr>
</table>
<div class="form-group">
	<?= Html::a('Send', ['/site/send', 'hu_number' => $model->hu_number], ['class' => 'btn btn-warning']); ?>
</div>
<?php ActiveForm::end(); ?>