<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;

?>

<?= Html::beginForm(['site/grid'], 'post'); ?>
<?php 
	$gridColumns = [
	    [
	        'class' => '\kartik\grid\SerialColumn'
	    ],
	    [
	        'class'       => '\kartik\grid\CheckboxColumn',
	        'pageSummary' => true,
	        'rowSelectedClass' => GridView::TYPE_DANGER	,
	    ],
	    'produk',
	    'description', 
	    'hu_number', 
	    'batch', 
	    'quantity',
    ];
	echo GridView::widget([
	    'dataProvider' 	=> $dataProvider,
	    //'filterModel'  	=> $searchModel,
	    'columns'      	=> $gridColumns,
	    'responsive'   	=>true,
	    'hover'        	=>true,
	    'pjax'         	=>true,
	    'striped'		=> true,
	    'pjaxSettings' 	=>[
	        'neverTimeout' => true,
	//      'beforeGrid'   => 'My fancy content before.',
	//      'afterGrid'    => 'My fancy content after.',
	    ],
	    'floatHeader'=>false,
	    'floatHeaderOptions'=>['scrollingTop'=>'50'],
	//  'showPageSummary' => true,
	]); 
?>
<?= Html::submitButton('Confirm', [ 'id' => 'update', 'class' => 'btn btn-success']) ?>
<?= Html::endForm(); ?>