<?php 

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;

$this->title = Yii::t('app', 'Dispatch');
$this->params['breadcrumbs'][] = $this->title;

?>
<?= Html::beginForm(['site/report'], 'post'); ?>
	<div class="form-group">
		<div class="row">
			<div class="col-md-3">
				<label>Order No</label>
				<input type="text" name="order" placeholder="Order Number" class="form-control" required>
			</div>
			<div class="col-md-4">
				<label>STO No</label>
				<?php 
				$sto = \common\models\MasterSto::find()->all();
				$listSto = \yii\helpers\ArrayHelper::map($sto, 'no_sto', 'no_sto');
				// Without model and implementing a multiple select
				echo Select2::widget([
				    'name' => 'no_sto',
				    'data' => $listSto,
				    'options' => [
				        'placeholder' => 'Pilih STO Number',
				        'multiple' => false
				    ],
				    'pluginOptions' => [
				        'allowClear' => true
				    ],
				]);
			?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-3">
				<label>No Truk</label>
				<input type="text" name="no_pol" placeholder="Masukan Nomor Truk" class="form-control" required>
			</div>
			<div class="col-md-3">
				<label>Jenis Truk</label>
				<?php 
				echo Select2::widget([
				    'name' => 'jenis_truk',
				    'data' => [
				    	'WINGBOX' => 'WINGBOX', 
				    	'FUSO' => 'FUSO',
				    	'CONTAINER' => 'CONTAINER'
				    ],
				    'options' => [
				        'placeholder' => 'Pilih Jenis Truck',
				        'multiple' => false
				    ],
				    'pluginOptions' => [
				        'allowClear' => true
				    ],
				]);
				?>
			</div>
			<div class="col-md-3">
				<label>Transporter</label>
				<?php 
				echo Select2::widget([
				    'name' => 'transporter',
				    'data' => [
				    	'PT CTL' => 'PT CTL', 
				    	'PT LANCAR' => 'PT LANCAR', 
				    	'PT PAREX' => 'PT PAREX', 
				    	'PT TJL' => 'PT TJL'
				    ],
				    'options' => [
				        'placeholder' => 'Pilih Jenis Truck',
				        'multiple' => false
				    ],
				    'pluginOptions' => [
				        'allowClear' => true
				    ],
				]);
				?>
			</div>
			<div class="col-md-3">
				<label>Nama Supir</label>
				<input type="text" name="nama_supir" placeholder="Nama Supir" class="form-control" required>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<label>Address line 1</label>
				<input type="text" name="address" placeholder="Alamat Tujuan" class="form-control" required>
			</div>
		</div>
	</div>
	<div class="form-group">
		<?= Html::submitButton('<i class="fa fa-print"></i>  Print', ['class' => 'btn btn-primary']) ?>	
	</div>
<?= Html::endForm() ?>