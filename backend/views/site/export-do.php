<?php 

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;

$this->title = Yii::t('app', 'Export Data Inbound');
$this->params['breadcrumbs'][] = $this->title;

?>

<?= Html::beginForm(['site/export_do'], 'post'); ?>
<p>Please fill out in sto number. We will take your requested.</p>
<div class="form-group">
	<div class="row">
		<div class="col-md-3">
			<label>Select Date</label>
			<?php $date = date("Y-m-d H:i:s"); ?>
			<?php
				echo yii\jui\DatePicker::widget( [
				// 'pluginOptions' => [
				// 'autoClose' => true,
				// 'format' => 'yyyy-mm-dd',
				// ],
				'options' => ['class' => 'form-control'],
				'value' => $date,
				'name' => '_date',
				'dateFormat' => 'yyyy-MM-dd',
				'clientOptions' =>[
					'numberOfMonths' => 1,
					//'minDate' => '-1m',
					//'maxDate' => '+1m',
					'changeMonth' => true,
					'changeYear' => true,
				]
			]) ?>
		</div>
	</div>
</div>
<div class="form-group">
	<div class="row">
		<div class="col-md-6">
			<label>Select No Refrence / PO / Truck</label>
			<?php 
				$do = \common\models\MasterDo::find()->all();
				$listDo = \yii\helpers\ArrayHelper::map($do, 'no_do', 'no_do');
				// Without model and implementing a multiple select
				echo Select2::widget([
				    'name' => 'no_do',
				    'data' => $listDo,
				    'options' => [
				        'placeholder' => 'Pilih No Refrence',
				        'multiple' => false
				    ],
				    'pluginOptions' => [
				        'allowClear' => true
				    ],
				]);
			?>
			<?php 
				
			?>
			<!--<input type="text" name="no_sto" class="form-control" required >-->
		</div>
	</div>
</div>
<div class="form-group">
	<?= Html::submitButton('<i class="fa fa-download"></i>  Export', ['class' => 'btn btn-primary']) ?>	
</div>

<?= Html::endForm() ?>