<?php 

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;

$this->title = Yii::t('app', 'Export Data Outbound');
$this->params['breadcrumbs'][] = $this->title;

?>

<?= Html::beginForm(['site/export'], 'post'); ?>
<p>Please fill out in sto number. We will take your requested.</p>
<div class="form-group">
	<div class="row">
		<div class="col-md-6">
			<?php 
				$sto = \common\models\MasterSto::find()->all();
				$listSto = \yii\helpers\ArrayHelper::map($sto, 'no_sto', 'no_sto');
				// Without model and implementing a multiple select
				echo Select2::widget([
				    'name' => 'no_sto',
				    'data' => $listSto,
				    'options' => [
				        'placeholder' => 'Pilih STO Number',
				        'multiple' => false
				    ],
				    'pluginOptions' => [
				        'allowClear' => true
				    ],
				]);
			?>
			<?php 
				
			?>
			<!--<input type="text" name="no_sto" class="form-control" required >-->
		</div>
	</div>
</div>
<div class="form-group">
	<?= Html::submitButton('<i class="fa fa-download"></i>  Export', ['class' => 'btn btn-primary']) ?>	
</div>

<?= Html::endForm() ?>