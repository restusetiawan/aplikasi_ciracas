<?php
use yii\helpers\Html;
use common\models\MasterData;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">
    <?= Html::a('<span class="logo-mini">APP</span><span class="logo-lg">KLOG - <small>Karpelindo</small></span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-folder-open-o"></i>  <i>Scan</i>
                        <?php 
                            $count = MasterData::find()->where(['status' => 'scan'])->count(); 
                            if ($count > 0) {
                                echo "<span class='label label-danger'>$count</span>";
                            } else {
                                echo "<span class='label label-default'>0</span>";
                            } 
                        ?>
                    </a>
                    <ul class="dropdown-menu">
                    
                        <li class="header">Please see your messages</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                <li><!-- start message -->
                                    <a href="<?php echo \yii\helpers\Url::to(['/site/data']) ?>">
                                        <div class="pull-left">
                                            <i class="fa fa-folder-open-o"></i>
                                        </div>
                                        <p>
                                        <?php 
                                        $count = MasterData::find()->where(['status' => 'scan'])->count(); 
                                        echo "Pallet yang sudah di scann = <b>$count</b> pallet"; ?>
                                        </p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-globe"></i>  <i>Shipment</i>
                        <?php 
                            $count = MasterData::find()->where(['status' => 'shipment'])->count(); 
                            if ($count > 0) {
                                echo "<span class='label label-danger'>$count</span>";
                            } else {
                                echo "<span class='label label-default'>0</span>";
                            } 
                        ?>
                    </a>
                    <ul class="dropdown-menu">
                    
                        <li class="header">Please see your messages</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                <li><!-- start message -->
                                    <a href="<?php echo \yii\helpers\Url::to(['/site/outbound']) ?>">
                                        <div class="pull-left">
                                            <b>FG</b>
                                        </div>
                                        <p>
                                        <?php 
                                        $count = MasterData::find()
                                            ->where(['status' => 'shipment', 'type_produk' => 'FG'])
                                            ->count(); 
                                        echo "Pallet yang sudah di shipment = <b>$count</b> pallet"; ?>
                                        </p>
                                    </a>
                                </li>
                                <li><!-- start message -->
                                    <a href="<?php echo \yii\helpers\Url::to(['/site/outbound_sugar']) ?>">
                                        <div class="pull-left">
                                            <b>GULA</b>
                                        </div>
                                        <p>
                                        <?php 
                                        $count = MasterData::find()
                                            ->where(['status' => 'shipment', 'type_produk' => 'GULA'])
                                            ->count(); 
                                        echo "Pallet yang sudah di shipment = <b>$count</b> pallet"; ?>
                                        </p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><?php echo \Yii::$app->user->identity->username; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-body">
                            <div class="col-xs-4 text-center">
                                <a href="#">Followers</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">Sales</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">Friends</a>
                            </div>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Sign out',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
