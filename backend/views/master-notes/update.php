<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterNotes */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Master Notes',
]) . $model->id_notes;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Notes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_notes, 'url' => ['view', 'id' => $model->id_notes]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="master-notes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
