<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MasterLokasi */

$this->title = Yii::t('app', 'Create Master Lokasi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Lokasis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-lokasi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
