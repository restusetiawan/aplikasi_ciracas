<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterRow */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Master Row',
]) . $model->id_row;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Rows'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_row, 'url' => ['view', 'id' => $model->id_row]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="master-row-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
