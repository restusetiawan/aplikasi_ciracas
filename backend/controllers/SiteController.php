<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\UploadForm;
use common\models\MasterData;
use common\models\MasterDataSearch;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
use common\models\SearchForm;
use kartik\mpdf\Pdf;
use yii\helpers\Url;
use common\models\Events;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
                            'logout', 
                            'index', 
                            'upload', 
                            'tree', 
                            'data', 
                            'confirm', 
                            'search', 
                            'send',
                            'outbound',
                            'outbound_sugar',
                            'export',
                            'putway',
                            'export_do',
                            'report',
                            'grid',
                            'shipment'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        //$searchModel = new EventsSearch();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $date = Yii::$app->request->post('datepicker');
            
        if (!is_null($date)) {
            $day = $date;
            $month = date("n",STRtotime($date));
        }else{
            $day = date("Y-m-d");
            $month = date("n");
        }

        $model = new MasterData();
        $status = 'confirm';
        $dataProviderFg = new ActiveDataProvider([
            'query' => $model->find()
                    ->andWhere(['status'=> $status])
                    ->andWhere(['DATE(created_at)' => $day])
                    ->distinct(),
        ]);

        $events = Events::find()->where(['publish' => 1])->all();
        $task = [];
        foreach ($events as $eve) {
            $event = new \yii2fullcalendar\models\Event();
            $event->id = $eve->id;
            //$event->backgroundColor = 'red';
            $event->title = $eve->title;
            $event->start = $eve->start_date;
            $event->end = $eve->end_date;

            $task[] = $event;
        }
        return $this->render('index', [
            //'searchModel' => $searchModel,
            //'dataProvider' => $dataProvider,
            'events' => $task,
            'dataProviderFg' => $dataProviderFg,
            'day' => $day,
        ]);
    }

    /**
     * Function Untuk action confirmation STO
     *
     * @return string
     */

    protected function findModel($id)
    {
        if (($model = MasterData::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionExport()
    {
        $model = new \common\models\MasterSto();
        $request = Yii::$app->request;
        $db = Yii::$app->db;

        $no_sto = $request->post('no_sto');

        if (!is_null($no_sto)) {
            $customer = "FFI";
            $customer_code = 5;
            $tanggal = date("d/m/Y");
            $separator = "\t";
            $filename = "data_".$no_sto.".txt";
            $dataSearch = new MasterDataSearch();
            $query = \common\models\MasterData::find()->where(['no_sto' => $no_sto])->all();

            foreach ($query as $data) {
                echo 
                $customer.$separator.
                $customer_code.$separator.
                $tanggal.$separator.
                $no_sto.$separator.
                $separator.$separator.
                $data['produk'].$separator.
                $data['hu_number'].$separator.
                $data['batch'].$separator.
                $data['quantity'].$separator.
                $data['no_pol'].$separator.
                $data['driver_name']."\r\n";
            }
            header('Content-type: text/plain');
            header('Content-Disposition: attachment; filename='.$filename);

            Yii::$app->end();      
        } 
        return $this->render('export', ['model' => $model]);
    }

    public function actionExport_do()
    {
        $model = new \common\models\MasterDo();
        $request = \Yii::$app->request;
        $db = \Yii::$app->db;

        $no_do = $request->post('no_do');
        $date = $request->post('_date');

        if (!is_null($no_do)) {
            $separator = "\t";
            $filename = "data_".$no_do.".txt";
            $query = \common\models\TabelPutway::find()
                ->where(['refrence' => $no_do])
                ->andWhere(['like', 'create_at', $date])
                ->all();

            foreach ($query as $data) {
                echo 
                $data['hu_number'].$separator.
                $data['to_lokasi'].$separator.
                $data['to_row'].$separator.
                $data['to_level'].$separator.
                $data['refrence'].$separator.
                $data['create_at'].$separator."\r\n";
            }
            header('Content-type: text/plain');
            header('Content-Disposition: attachment; filename='.$filename);

            Yii::$app->end();      
        }
        return $this->render('export-do', ['model' => $model]);
    }
    
    public function actionUpload()
    {
        $model = new UploadForm();

        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'file');
            $filename = 'Data.'.$file->extension;
            $upload = $file->saveAs('uploads/'.$filename);

            if ($upload){
                define('CSV_PATH', 'uploads/');

                $csv_file = CSV_PATH.$filename;
                $filecsv = file($csv_file);
                print_r($filecsv);
                //die;

                foreach ($filecsv as $data) {

                    $modelnew = new MasterData();

                    $hasil = explode(",", $data);
                    $activity = $hasil[0];
                    $type = $hasil[1];
                    $produk = $hasil[2];
                    $description = $hasil[3];
                    $batch = $hasil[4];
                    $hu_number = $hasil[5];
                    $quantity = $hasil[6];
                    //$no_pol =  $hasil[7];
                    //$driver = $hasil[8];
                    $hu_number_key = substr($hu_number, -6);
                    $status = 'new';

                    $modelnew->activity  = $activity;
                    $modelnew->type_produk = $type;
                    $modelnew->produk = $produk;
                    $modelnew->description = $description;
                    $modelnew->batch  = $batch;
                    $modelnew->hu_number = $hu_number;
                    $modelnew->quantity = $quantity;
                    //$modelnew->no_pol = $no_pol;
                    //$modelnew->driver_name = $driver;
                    $modelnew->hu_number_key = $hu_number_key;
                    $modelnew->status = $status;

                    $modelnew->save(); 
                }

                unlink('uploads/'.$filename);
                Yii::$app->session->setFlash('success', '<span class="glyphicon glyphicon-info-sign"></span>  <b>Data Success di Upload</b>');
                return $this->redirect(['master-data/index']);
            } 
        } elseif (Yii::$app->request->isAjax) {
            return $this->renderAjax('upload', [
                'model' => $model,
            ]);
        }else {
            return $this->render('upload', [
                'model' => $model,
            ]);
        } 
    }

    public function actionSearch()
    {
        $searchModel = new MasterDataSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new MasterData();

        return $this->render('search', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
    }

    public function actionShipment()
    {
        $searchModel = new MasterDataSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new MasterData();
        if (isset($_REQUEST['selection'])) {
            $status = 'shipment';
            $no_pol = Yii::$app->request->post('no_pol');
            $nama_supir = Yii::$app->request->post('driver_name');

            foreach ($_REQUEST['selection'] as $index => $checkbox) {
                $check = ($checkbox);
                $update = "UPDATE master_data SET status='$status', no_pol='$no_pol', driver_name='$nama_supir' WHERE id='$check' ";
                $query = Yii::$app->db->createCommand($update)->execute();
            }
            //Yii::$app->session->setFlash('success', 'Sukses Melakukan Shipment confirmation');
            $this->redirect(['/site/shipment']);
        }
        return $this->render('shipment', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
    }

    public function actionSend($hu_number)
    {
        $model = MasterData::find()->where(['hu_number' => $hu_number])->one();
        $count = MasterData::find()->where(['status' => 'scan'])->count();
        $status = 'scan';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Sukses Kirim data');

            return $this->redirect(['/site/search']);
        } else {
            return $this->render('create', [
                    'model' => $model,
                    $model->status = $status,
                ]);
        }
    }

    /**
     * Function untuk menampilkan data dari table master_data
     *
     * @return string
     */
    public function actionData()
    {
        $data = MasterData::find()->where(['status' => 'scan']);
        $dataProvider = new ActiveDataProvider([
            'query' => $data,
        ]);
        
        if (isset($_REQUEST['selection'])) {
            $status = 'confirm';
            //$no_pol = Yii::$app->request->post('no_pol');
            //$nama_supir = Yii::$app->request->post('driver_name');
            $no_sto = Yii::$app->request->post('no_sto');
            foreach ($_REQUEST['selection'] as $index => $checkbox) {
                $check = ($checkbox);
                $update = "UPDATE master_data SET status='$status', no_sto='$no_sto' WHERE id='$check' ";

                $query = Yii::$app->db->createCommand($update)->execute();
            }
            if ($query) {
                $modelSto = new \common\models\MasterSto();
                $modelSto->no_sto = $no_sto;
                $modelSto->save();
            }
            $this->redirect(['/site/data']);
        }
        return $this->render('data', [
                'dataProvider' => $dataProvider,
                //'pages' => $pages,
            ]);
    }

    public function actionOutbound()
    {
        $query = MasterData::find()->where(['activity' => 'outbound', 'type_produk' => 'FG', 'status' => 'shipment']);
        //$countQuery = clone $query;
        //$pagesFg = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        //$modelsFg = $query->offset($pagesFg->offset)
        //    ->limit($pagesFg->limit)
        //    ->all();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (isset($_REQUEST['selection'])) {
            $status         = 'done';
            $value          = range(0, 9);
            shuffle($value);
            $get_value      = array_rand($value, 6);
            $value_str      = implode("", $get_value);
            $code           = $value_str;

            foreach ($_REQUEST['selection'] as $index => $checkbox) {
                $check = ($checkbox);
                $update = "UPDATE master_data SET status='$status', order_no='$code' WHERE id='$check' ";

                $query = Yii::$app->db->createCommand($update)->execute();
            }
            $this->redirect(['/site/outbound']);
        }
        return $this->render('outbound', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionOutbound_sugar()
    {
        $query = MasterData::find()->where(['activity' => 'outbound', 'type_produk' => 'gula', 'status' => 'shipment']);
        //$countQuery = clone $query;
        //$pagesSugar = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        //$modelsSugar = $query->offset($pagesSugar->offset)
        //   ->limit($pagesSugar->limit)
        //    ->all();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (isset($_REQUEST['selection'])) {
            $status         = 'done';
            /*$create_at = date("Y-m-d");
            $today = date("dmy");
            $countSto = \common\models\MasterSto::find()->where(['LIKE', 'create_at', $create_at])->count();
            $kp = "KP";
            $code = $kp.$today.'00'.$countSto+1;*/
            $value          = range(0, 15);
            shuffle($value);
            $get_value      = array_rand($value, 6);
            $value_str      = implode("", $get_value);
            $date           = date("dmy");
            $time           = time("s");
            $code           = "KP".$value_str;
            //$no_sto = Yii::$app->request->post('no_sto');
            //$nama_supir = Yii::$app->request->post('nama_supir');
            foreach ($_REQUEST['selection'] as $index => $checkbox) {
                $check = ($checkbox);
                $update = "UPDATE master_data SET status='$status', order_no='$code' WHERE id='$check' ";

                $query = Yii::$app->db->createCommand($update)->execute();
            }
            if ($query) {
                Yii::$app->session->setFlash('success', 'Sukses melakukan Konfirmasi, Silahkan Masuk ke Menu Dispatch');
            }
            return $this->render('sukses', ['code' => $code]);
        }
        return $this->render('outbound-sugar', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPutway()
    {   
        $model = new \common\models\TabelPutway();

        if ($model->load(Yii::$app->request->post())) {
            $status = 'scan';
            $model->status = $status;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Sukses Putway');
                return $this->redirect(['/site/putway']);
            }
        } else {
            return $this->render('/tabel-putway/create', [
                'model' => $model,
            ]);
        }
    }

    public function actionReport()
    {
        $date = date("d/m/Y H:i:s");
        $order = Yii::$app->request->post('order');
        $no_sto = Yii::$app->request->post('no_sto');
        $no_pol = Yii::$app->request->post('no_pol');
        $jenis_truk = Yii::$app->request->post('jenis_truk');
        $transporter = Yii::$app->request->post('transporter');
        $nama_supir = Yii::$app->request->post('nama_supir');
        $address = Yii::$app->request->post('address');

        $model = MasterData::find()
            ->where(['order_no' => $order])
            ->all();

        $content = $this->renderPartial('pdfreport', [
           'models' => $model,
           'order' => $order,
           'no_sto' => $no_sto,
           'no_pol' => $no_pol,
           'jenis_truk' => $jenis_truk,
           'transporter' => $transporter,
           'nama_supir' => $nama_supir,
           'address' => $address,
        ]);

        if (!is_null($order)) {
            // setup kartik\mpdf\Pdf component
            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_BLANK,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $content, 
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
                 // call mPDF methods on the fly
                'methods' => [
                    'SetHeader'=>['<i>Reporting Delivery Notes</i>'.''.'-'.''.$date],
                    'SetFooter'=>['{PAGENO}'],
                ]
            ]);
     
            // http response
            $response = Yii::$app->response;
            $response->format = \yii\web\Response::FORMAT_RAW;
            $headers = Yii::$app->response->headers;
            $headers->add('Content-Type', 'application/pdf');
            // return the pdf output as per the destination setting
            return $pdf->render();
        }

        return $this->render('dispatch', [ 
            'model' => $model, 
        ]);
    }

    public function actionGrid()
    {
        //$searchModel = new MasterDataSearch();
        $model = MasterData::find();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider = new ActiveDataProvider([
            'query' => $model,
        ]);

        if (isset($_REQUEST['selection'])) {
            $status = 'done';
            $produk = 66666;
            foreach ($_REQUEST['selection'] as $key => $value) {
                $check = ($value);
                $update = "UPDATE master_data SET status='$status', produk='$produk' WHERE id='$check' ";

                $query = Yii::$app->db->createCommand($update)->execute();
            }
        }
        return $this->render('/site/grid', [
                'dataProvider' => $dataProvider,
                //'searchModel' => $searchModel,
            ]);
        
    }
}