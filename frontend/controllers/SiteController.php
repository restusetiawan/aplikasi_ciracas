<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\UploadForm;
use yii\web\UploadedFile;
use common\models\MasterData;
use common\models\MasterDataSearch;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'data', 'search', 'putway', 'putway_confirmation', 'index', 'shipment'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'data', 'search', 'putway','putway_confirmation', 'index', 'shipment'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        //return $this->goHome();
        return $this->redirect(['/site/login']);
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionUpload()
    {
        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');

            if ($model->validate()) {
                $model->file->saveAs('uploads/'. $model->file->baseName.'.'.$model->file->extension);
            }
        }
        return $this->render('upload', [
            'model' => $model,
        ]);
    }

    public function actionSearch()
    {
        $searchModel = new \common\models\MasterDataSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new \common\models\MasterData();

        return $this->render('search', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
    }

    public function actionSend($hu_number)
    {
        $model = \common\models\MasterData::find()->where(['hu_number' => $hu_number])->one();
        $count = \common\models\MasterData::find()->where(['status' => 'scan'])->count();
        $status = 'scan';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Sukses Kirim data');

            return $this->redirect(['/site/search']);
        } else {
            return $this->render('create', [
                    'model' => $model,
                    $model->status = $status,
                ]);
        }
    }

    public function actionData()
    {
        $data = \common\models\MasterData::find()->where(['status' => 'scan']);
        $countQuery = clone $data;
        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $models = $data->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        
        if (isset($_POST['checkbox'])) {
            $status = 'confirm';
            //$no_pol = Yii::$app->request->post('no_pol');
            //$nama_supir = Yii::$app->request->post('driver_name');
            $no_sto = Yii::$app->request->post('no_sto');
            foreach ($_POST['checkbox'] as $index => $checkbox) {
                $check = ($checkbox);
                $update = "UPDATE master_data SET status='$status', no_sto='$no_sto' WHERE id='$check' ";

                $query = Yii::$app->db->createCommand($update)->execute();
            }
            if ($query) {
                $modelSto = new \common\models\MasterSto();
                $modelSto->no_sto = $no_sto;
                $modelSto->save();
            }
            $this->redirect(['/site/data']);
        }
        return $this->render('data', [
                'models' => $models,
                'pages' => $pages,
            ]);
    }
    public function actionPutway()
    {   
        $model = new \common\models\TabelPutway();

        if ($model->load(Yii::$app->request->post())) {
            $status = 'scan';
            $model->status = $status;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Sukses Putway');
                return $this->redirect(['/site/putway']);
            }
        } else {
            return $this->render('@backend/views/tabel-putway/create', [
                'model' => $model,
            ]);
        }
    }

    public function actionPutway_confirmation()
    {
        $models = \common\models\TabelPutway::find()->where(['status' => 'scan'])->all();
        //$count = clone $data;
        //$pages = new \yii\data\Pagination(['totalCount' => $count->count()]);
        //$models = $data->offset($pages->offset)
          //  ->limit($pages->limit)
            //->all();

        if (isset($_POST['checkbox'])) {
            $status = 'done';
            $refrence = Yii::$app->request->post('refrence');
            foreach ($_POST['checkbox'] as $key => $checkbox) {
                $check = ($checkbox);
                $update = "UPDATE tabel_putway SET status = '$status', refrence = '$refrence' WHERE putway_id = '$check' ";

                $query = \Yii::$app->db->createCommand($update)->execute();
            }
            if ($query) {
                $modelDo = new \common\models\MasterDo();
                $modelDo->no_do = $refrence;
                $modelDo->save();
            }
            $this->redirect(['/site/putway_confirmation']);
        }
        return $this->render('putway', [
                'models' => $models,
                //'pages' => $pages,
            ]);
    }

    public function actionEdit($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $status = 'scan';
            $model->status = $status;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Update Data Sukses');
                return $this->redirect(['putway_confirmation']);
            }
        } else {
            return $this->render('@backend/views/tabel-putway/update', [
                'model' => $model,
            ]);
        }
    }

    protected function findModel($id)
    {
        if (($model = \common\models\TabelPutway::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionShipment()
    {
        $searchModel = new MasterDataSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new MasterData();
        if (isset($_REQUEST['selection'])) {
            $status = 'shipment';
            $no_pol = Yii::$app->request->post('no_pol');
            $nama_supir = Yii::$app->request->post('driver_name');

            foreach ($_REQUEST['selection'] as $index => $checkbox) {
                $check = ($checkbox);
                $update = "UPDATE master_data SET status='$status', no_pol='$no_pol', driver_name='$nama_supir' WHERE id='$check' ";
                $query = Yii::$app->db->createCommand($update)->execute();
            }
            //Yii::$app->session->setFlash('success', 'Sukses Melakukan Shipment confirmation');
            $this->redirect(['/site/shipment']);
        }
        return $this->render('shipment', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
    }
}
