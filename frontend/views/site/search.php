<?php 

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

$this->title = Yii::t('app', 'Search');
//$this->params['breadcrumbs'][] = $this->title;

?>
<h1><?= \yii\helpers\Html::encode($this->title); ?></h1>
<?php echo $this->render('_search', ['model' => $searchModel]); ?>
<?php 
	if($searchModel->hu_number != null){
		echo GridView::widget([
					'dataProvider' => $dataProvider,
					// 'filterModel' => $searchModel,
					'columns' => [
						[						
							'class' => 'yii\grid\ActionColumn',
							'header' => 'Actions',
							'buttons' => [
								'checkin' => function ($url,$model) {
									return Html::a(
										'<p>Next>></p>',
										//without delete action
										//['/checkin/checkin_security','transporter'=>$model->no_truck],
										//---------------------
										['/site/send','hu_number'=>$model->hu_number],
										[
											'title' => 'Check-IN',
										]
									);
								},
							],
							'template' => '{checkin}',
						],
							'produk',
							//'description',
							'quantity',
					],
				]);	
	}
	else{
		Yii::$app->session->setFlash('warning', 'Please Masukan HU Number');
	}
?>