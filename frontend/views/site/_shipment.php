<?php 

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
	'action' => ['shipment'], 
	'method' => 'get',
]); ?>

<?= $form->field($model, 'no_sto')->textInput(['class' => 'form-control', 'placeholder' => 'Masukan STO Number']) ?>
<div class="form-group">
	<?= Html::submitButton('Search', ['class' => 'btn btn-primary']); ?>
</div>

<?php ActiveForm::end(); ?>