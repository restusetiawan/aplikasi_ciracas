<?php 

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;

$this->title = Yii::t('app', 'Putway Confirm');
//$this->params['breadcrumbs'][] = $this->title;
?>
<script type="text/javascript">
	function toggle(pilih) {
	  	checkboxes = document.getElementsByName('checkbox[]');
	  	for (var i = 0, n=checkboxes.length;i<n;i++) {
	    	checkboxes[i].checked = pilih.checked;
	  	}
	}
</script>
<h1><?= \yii\helpers\Html::encode($this->title); ?></h1>
<?php $form = ActiveForm::begin() ?>
<div class="form-group">
	<input type="text" class="form-control" name="refrence" placeholder="Refrence" required >
</div>
<table class="table table-bordered table-hover table-striped">
	<thead>
		<th><input type="checkbox" name="checkboxall" onClick="toggle(this)"></th>
		<th>No</th>
		<th>Type Produk</th>
		<th>HU Number</th>
		<th>To Lokasi</th>
		<th>action</th>
	</thead>
	<tbody>
	<?php 
		$no = 1;
		foreach ($models as $model) { ?>
			<tr>
				<td><input type="checkbox" name="checkbox[]" id="checkbox" value="<?php echo $model['putway_id']; ?>" /></td>
				<td><?php echo $no; ?></td>
				<td><?php echo $model->type_produk; ?></td>
				<td><?php echo $model->hu_number; ?></td>
				<td><?php echo $model->to_lokasi.'-'.$model->to_row.$model->to_level; ?></td>
				<td><?= Html::a('edit',['/site/edit', 'id' => $model->putway_id]); ?></td>
			</tr>
		<?php $no++; } ?>
	</tbody>
</table>
<div class="form-group">
	<?= Html::submitButton('<i class="fa fa-spinner"></i>  Confirm', [ 'id' => 'update', 'class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end() ?>