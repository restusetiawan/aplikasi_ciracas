<?php 

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;

$this->title = Yii::t('app', 'Data Scann');
//$this->params['breadcrumbs'][] = $this->title;
?>
<script type="text/javascript">
	function toggle(pilih) {
	  	checkboxes = document.getElementsByName('checkbox[]');
	  	for (var i = 0, n=checkboxes.length;i<n;i++) {
	    	checkboxes[i].checked = pilih.checked;
	  	}
	}
</script>
	<?php /*if (empty($models)) {
		echo "<div class='alert alert-danger'>
				<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
				<strong>Data Not Result</strong>
			</div>";
	}*/ ?>
	<h1><?= \yii\helpers\Html::encode($this->title); ?></h1>
	<?php $form = ActiveForm::begin(); ?>
		<div class="form-group">
			<?php 
				/*$truck = \common\models\Transporter::find()->all();
				$listTruck = \yii\helpers\ArrayHelper::map($truck, 'no_pol', 'no_pol');

				echo Select2::widget([
					'name' => 'no_pol',
					'data' => $listTruck,
					'value' => '',
					'options' => ['placeholder' => 'Pilih No Polisi'],
					'pluginOptions' => ['allowClear' => true],
				])*/
			?>
			<!--<label>No Truck</label>
			<input type="text" class="form-control" name="no_pol" placeholder="No Truck" required>
		</div>
		<div class="form-group">
			<label>Nama Supir</label>
			<input type="text" class="form-control" name="driver_name" placeholder="Nama Supir" required>
		</div>-->
		<div class="form-group">
			<label>No STO</label>
			<input type="text" class="form-control" name="no_sto" placeholder="No STO" required>
		</div>
		<table class="table table-bordered table-hover table-striped">
			<thead>
				<th><input type="checkbox" name="checkboxall" onClick="toggle(this)"></th>
				<th>No</th>
				<!--<th>Produk</th>-->
				<!--<th>Decription</th>-->
				<th>Hu Number</th>
				<th>Batch</th>
				<th>Quantity</th>
			</thead>
			<tbody>
				<?php
				$no = 1;
				foreach ($models as $model) {  ?>
					<tr id="rows[]">
						<td>
							<input type="checkbox" name="checkbox[]" id="checkbox" value="<?php echo $model['id']; ?>" />
						</td>
						<td><?php echo $no; ?></td>
						<!--<td>
							<?php //echo $model->produk; ?>
							<?php //echo $form->field($model, 'produk[]')->hiddenInput([
								//'value' => $model['produk'],
								//'readOnly' => true
							//])->label(false) ?>
						</td>-->
						<!--<td>
							<?php //echo $form->field($model, 'description[]')->textInput([
								//'value' => $model['description'],
								//'readOnly' => true
							//])->label(false) ?>
						</td>-->
						<td>
							<?php echo $model->hu_number; ?>
							<?php echo $form->field($model, 'hu_number[]')->hiddenInput([
								'value' => $model['hu_number'],
								'readOnly' => true
							])->label(false) ?>
						</td>
						<td>
							<?php echo $model->batch; ?>
							<?php echo $form->field($model, 'batch[]')->hiddenInput([
								'value' => $model['batch'],
								'readOnly' => true
							])->label(false) ?>
						</td>
						<td>
							<?php echo $model->quantity; ?>
							<?php echo $form->field($model, 'quantity[]')->hiddenInput([
								'value' => $model['quantity'],
								'readOnly' => true
							])->label(false) ?>
						</td>
					</tr>
					
				<?php $no++; } ?>
			</tbody>
		</table>
		<div class="form-group">
        	<?= Html::submitButton('<i class="fa fa-spinner"></i>  Confirm', [ 'id' => 'update', 'class' => 'btn btn-primary']) ?>
    	</div>
	<?php ActiveForm::end() ?>
	<?php 
		echo \yii\widgets\LinkPager::widget([
			'pagination' => $pages,
		]);
	?>
